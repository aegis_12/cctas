
var deployer = require('./Utils/runtime.js')

var component = require('./component.json')

deployer.Ready(component).then(function(deployer_interface) 
{
	console.log("All ready")
	// Receive message from frontend clients
	deployer_interface.on('in_recv', function(msg)
	{
		console.log("-----------------")
		console.log(msg)
		// Select a worker to send the data
		// compose new message
		new_msg = {'clientID':msg['clientID'], 'data':'beta'}
		// send it back
		deployer_interface.emit('in_send', new_msg)
	})
}, 
function(err)
{
	console.log("some error raised " + err)
})