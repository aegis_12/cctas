
var exports = module.exports = {};

// Return only the hour:minutes:seconds
function getDate () 
{
	return new Date().toJSON().slice(11,19)
}

// Generic function to compose the message
function composeMessage (idMessge, msg) 
{
	return getDate() + " [" + idMessge + "]: " + msg
}

exports.info = function(msg)
{
	console.log(composeMessage('INFO', msg))
};