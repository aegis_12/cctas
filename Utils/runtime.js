
// RUNTIME

// Requires
var comcat = require('./comcat.js')
var utils  = require('./utils.js')
var events = require('events');
var zmq = require('zmq')

var exports = module.exports = {};

function alt () {
	return JSON.stringify({
		'code': 5,
		'skct': 'aa'
	})
}
// Connections

var Connections = {}
Connections['server'] 		= new events.EventEmitter();
Connections['interface'] 	= new events.EventEmitter();

var undefined_interfaces = {}

var identity = undefined
// export
// All connected
exports.Ready = function(component)
{
	return new Promise(function(resolve, reject)
	{
		// read component.json and do stuff
		// read component.json
		// var component = require('../component.json')

		console.log(component)

		// Identity
		identity = utils.compose_Random_Identity(component['name'])
		console.log('ID: ' + identity)

		// Create undefined interfaces variable from component.json
		// Format: {interface1_name: INTERFACE_NOT_DEFINED, interface2_name: INTERFACE_NOT_DEFINED}

		for (var item in component['interfaces'])
		{
			undefined_interfaces[component['interfaces'][item]] = INTERFACE_NOT_DEFINED
		}
		console.log("Interface file: " + undefined_interfaces)

		// Connect with server
		data = {'identity': identity, 'port': '9000', 'interface': 'server'}
		// Connect to Main server
		comcat.get_socket_zmq().dealer_router_HB(data, Connections['server']).then(function()
		{
			console.log("Worker conectado con el servidor principal")
		}, 
		function(err)
		{
			console.log("not connected to server TIMEOUT: " + err)
		})

		// return a promise with the reference of the Connections interface
		utils.allConnected(undefined_interfaces).then(function()
		{
			console.log("....AA....")
			resolve(Connections['interface'])
		})
	})
}

// returns a promise
// takes each one of the interfaec and create a socket
// in form of a primse
// {"interface":"in","socket":"router_dealer","port":6000}
function createSocketFromInterface(data)
{
	var p
	// Depends..
	// console.log(data)
	data['identity'] = identity
	console.log("--DATA--")
	console.log(data)
	// search for the function
	// p1 = comcat.get_socket_zmq()[data['socket']]
	// p  = p1(data, Connections['interface'])
	/*
	if (data['socket'] == 'router_dealer') 
	{
		// p = comcat.get_socket_zmq().router_dealer(data['port'], Connections['interface'], data['interface'])
		p = comcat.get_socket_zmq().router_dealer(data, Connections['interface'])
	}
	else if (data['socket'] == 'router_dealer_HB') 
	{
		// p = comcat.get_socket_zmq().router_dealer_HB(data['port'], Connections['interface'], data['interface'])
		p = comcat.get_socket_zmq().router_dealer_HB(data, Connections['interface'])
	}
	else if (data['socket'] == 'dealer_router')
	{
		// p = comcat.get_socket_zmq().dealer_router(data['port'], identity, Connections['interface'], data['interface'])
		p = comcat.get_socket_zmq().dealer_router(data, Connections['interface'])
	}
	else if (data['socket'] == 'dealer_router_HB') 
	{
		// p = comcat.get_socket_zmq().dealer_router_HB(data['port'], identity, Connections['interface'], data['interface'])
		p = comcat.get_socket_zmq().dealer_router_HB(data, Connections['interface'])
	}
	*/
	return comcat.get_socket_zmq()[data['socket']](data, Connections['interface'])
}

// Server -> worker
function confirm_settings() 
{
	return {
		'code': CONFIRM_SETTING
	}
}

// Start listening connections
Connections['server'].on('server_recv', function(data)
{
	var socket_promises = []
	// data = JSON.parse(data_raw)	
	if (data['code'] == SETTING_DEFINITION) 
	{
		for (var item in data['skct']) 
		{
			socket_promises.push(createSocketFromInterface(data['skct'][item]))
		}
		// createSocketFromInterface(data['skct'][1])
	}
	
	// now wait for all the promises 
	Promise.all(socket_promises).then(function() 
	{ 
		console.log("all defined correctly")
		// need to change the interfaces to run the code...
		for (var item in data['skct']) 
		{
			// console.log(data['skct'][item]['interface'])
			undefined_interfaces[data['skct'][item]['interface']] = INTERFACE_DEFINED
			// data['skct'][item]['interface']
		}
		// Change all the interfaces
		Connections['server'].emit('server_send', confirm_settings())
		// Connections['server'].emit('server_send', confirm_settings())
		// Send message back that all is ok
	}, function(){
		console.log("ERRERERER")
	});

	//if (data['code'] == SETTING_DEFINITION) {
	// for (var key in data1) {
	// 	console.log(key)
	// }
	// console.log(Object.keys(data1))
	//}
	/*
	// console.log(data)
	if (data['code'] == SETTING_DEFINITION)
	{
		// Check interface here...
		// Check if interface left...
		var p = undefined
		// Check if interface already defined...
		if (data['zmq'] == 'subscriber')
		{
			p = comcat.get_socket_zmq().subscriber(data['addr'], data['tag'], Connections[data['event']], data['interface'])
		}
		else if (data['zmq'] == 'router_dealer') 
		{
			p = comcat.get_socket_zmq().router_dealer(data['addr'], Connections[data['event']], data['interface'])
		}
		else if (data['zmq'] == 'router_dealer_HB')
		{
			p = comcat.get_socket_zmq().router_dealer_HB(data['addr'], Connections[data['event']], data['interface'])
		}
		else if (data['zmq'] == 'dealer_router_HB')
		{
			p = comcat.get_socket_zmq().dealer_router_HB(data['addr'], identity, Connections[data['event']], data['interface'])
		}
		else if (data['zmq'] == 'dealer_router')
		{
			p = comcat.get_socket_zmq().dealer_router(data['addr'], identity, Connections[data['event']], data['interface'])
		}
		else if (data['zmq'] == 'push_socket')
		{
			p = comcat.get_socket_zmq().push_socket(data['addr'], Connections[data['event']], data['interface'])
		}
		else if (data['zmq'] == 'pull_server')
		{
			p = comcat.get_socket_zmq().pull_server(data['addr'], Connections[data['event']], data['interface'])
		}
		else if (data['zmq'] == 'publisher')
		{
			p = comcat.get_socket_zmq().publisher(data['addr'], Connections[data['event']], data['interface'])
		}
		// Now call that promise and check result
		p.then(function (argument) {
			console.log("good")
			
			undefined_interfaces[data['interface']] = INTERFACE_DEFINED

			// send back to the server. that the connection was correct

		}, function (err) {
			console.log("bad")
		})
	}
	*/
})