
// Maybe is the last comcat i define here...

// Requires

var zmq = require('zmq')

var exports = module.exports = {};

// Global variables

// Client to Server

global.HANDSHAKE = 1

global.ALIVE = 2

global.OTHER_DATA = 3

// Server to Worker

global.CONFIRM_WORKER = 4

global.SETTING_DEFINITION = 5

global.CONFIRM_SETTING = 6

// Message definitions in JSON format to send through the channels

// Server -> worker
function confirm_worker() 
{
	return JSON.stringify
	({
		'code': CONFIRM_WORKER
	})
}

// Server -> worker
function confirm_settings() 
{
	return JSON.stringify
	({
		'code': CONFIRM_SETTING
	})
}

// Worker -> Server
function handshake_message() 
{
	return JSON.stringify
	({
		'code': HANDSHAKE
	})
}

// Worker -> Server
function still_alive_message() 
{
	return JSON.stringify
	({
		'code': ALIVE
	})
}

// Server -> Worker
// All of them defined in the same class/object
// Information to send to the client to set up configurations
// This functions defines many types of interfaces
// The clinet will map this definitions with functions to run the code

function zmq_def(socket_name, interface_name, addr)
{
    return JSON.stringify
    ({
    	'code'		: SETTING_DEFINITION,
    	'zmq' 		: socket_name,
    	'addr'		: addr,
    	'event' 	: 'interface',
    	'interface'	: interface_name
    })
}

function compose_message(socket_name, addr, interface_name)
{
//     return JSON.stringify
    ({
    	'code'		: SETTING_DEFINITION,
    	'zmq' 		: socket_name,
    	'addr'		: addr,
    	'event' 	: 'interface',
    	'interface'	: interface_name
    })
}

// To be able to export the data
exports.get_zmq_definitions = function()
{
	return zmq_definitions
};

// helper functions for the promises of clients-servers
function connection_promise(socket, resolve_name, reject_name){
	return new Promise(function(resolve, reject){
		// Good connection
		socket.on(resolve_name, function(){
			resolve()
		})
		// not good connection
		socket.on(reject_name, function(){
			console.log("NO HI HA RES!!")
			reject()
		})
		// Start monitor
		socket.monitor(500, 0);
	})
}

// resolve connect
// reject  close. connect, close
function client_promise(socket)
{
	return connection_promise(socket, 'connect', 'close')
}

// resolve with listen
// reject with bad bind. listen, bind_error
function server_promise(socket)
{
	return connection_promise(socket, 'listen', 'bind_error')
}

// Client
// Socket Creation
// Main function of the client. Will use the socket zmq definition in
// zmq_definitions to set up sockets. It is also defined in a single class/object
// A way to having all of them in a single place

var socket_zmq = 
{
	// push socket
	// push_socket: function(addr, server_event, interface_name)
	push_socket: function(data, server_event)
	{
		var socket = zmq.socket('push');

		var prom = client_promise(socket)

		socket.connect('tcp://localhost:' + data['port']);
		// receive emitter
		server_event.on(data['interface'] + '_send', function(msg)
		{
			socket.send(JSON.stringify(msg))
		})

		return prom
	},
	// pull_server: function(addr, server_event, interface_name)
	pull_server: function(data, server_event)
	{
		var socket = zmq.socket('pull');
		var prom = server_promise(socket)
		socket.bindSync('tcp://*:' + data['port']);
		// console.log('Worker connected to port 3000');

		socket.on('message', function(msg){
			// send to serverevent
			server_event.emit(data['interface'] + '_recv', JSON.parse(msg))
		});

		return prom
	},
	// publisher: function(addr, server_event, interface_name)
	publisher: function(data, server_event)
	{
		var sock = zmq.socket('pub');

		// console.log('Publisher bound to port 3000');
		var prom = server_promise(sock)

		sock.bindSync('tcp://*:' + data['port']);
		/*
		setInterval(function(){
		  console.log('sending a multipart message envelope');
		  sock.send(['kitty cats', 'meow!']);
		}, 500);*/
		server_event.on(data['interface'] + '_send', function(tag, msg)
		{
			sock.send([tag, JSON.stringify(msg)])
		})

		return prom
	},
	// Subscriber [RECEIVE]
	// subscriber: function(addrs, tag, server_event, interface_name)
	subscriber: function(data, server_event)
	{
		var subscriber = zmq.socket('sub')
		// Register to monitoring events
		var prom = client_promise(subscriber)
		// connect subscriber and add the tag
		subscriber.connect('tcp://localhost:' + data['port']);
		subscriber.subscribe('beta')
		// Read the messages and send them to server_event
		subscriber.on('message', function(data1, dato2){
			// data = JSON.parse(data1)
			server_event.emit(data['interface'] + '_recv', JSON.parse(dato2))
		})
		// 
		return prom
	},
	// simple router
	// router_dealer: function(addr, server_event, interface_name) 
	router_dealer: function(data, server_event)
	{
		var router = zmq.socket('router')
		var prom = server_promise(router)

		console.log("router-dealer " + data['port'])
		router.bindSync('tcp://*:' + data['port'])
		// handle send and receive..
		// receive
		router.on('message', function()
		{
			var args = Array.apply(null, arguments)
			// Like we receive from dealer. check index [0] and [1]
			var comp_id = args[0].toString()
			var msgJson = JSON.parse(args[1].toString())
			// console.log("-------------------")
			// console.log(args[1].toString())
			// console.log(msgJson)
			// console.log(msgJson['code'])		
			// Extract id and message
			// server_event.emit(interface_name+'_recv', comp_id, msgJson)
			server_event.emit(data['interface'] + '_recv', comp_id, msgJson)
		})
		// send
		server_event.on(data['interface']+'_send', function(comp_id, msg)
		{
			router.send([comp_id, JSON.stringify(msg)])
		})

		return prom
	},
	// router with heartbeats
	// router_dealer_HB: function(addr, server_event, interface_name)
	router_dealer_HB: function(data, server_event)
	{
		var router = zmq.socket('router')
		console.log("Connecting hb to .. " + data['port'])
		var prom = server_promise(router)
		router.bindSync('tcp://*:' + data['port']);
		// broker already handles that receive
		var broker = exports.compose_Broker(router, server_event, data['interface'])
		// The events of addWorker will be handled
		// by the client now, because we are using the other server event
		// Receive functions are already redirected with schema (component id, message)
		// and event interface_name + '_recv'
		// handle the calls to the server...
		server_event.on(data['interface']+'_send', function(comp_id, msg)
		{
			router.send([comp_id, JSON.stringify(msg)])
		})

		return prom
	},
	// client of the load balancer
	// dealer_router: function(addr, identity, server_event, interface_name)
	dealer_router: function(data, server_event)
	{
		// create the promise
		var worker = zmq.socket('dealer')
		// Register to monitoring events
		// worker.on('connect', function(fd, ep) {console.log('connect, endpoint:', ep);});
		// worker.on('connect_delay', function(fd, ep) {console.log('connect_delay, endpoint:', ep);});
		// worker.on('connect_retry', function(fd, ep) {console.log('connect_retry, endpoint:', ep);});
		var prom = client_promise(worker)
		// worker.monitor(500, 0);
		console.log("Connect dealer-router " + data['port'])
		worker.identity = data['identity']
		worker.connect('tcp://localhost:' + data['port'])
		// Receive. it is directly from the load balancer. only a msg
		worker.on('message', function(msg)
		{
			server_event.emit(data['interface'] + '_recv', JSON.parse(msg))
		})
		// worker.send(JSON.stringify({'code':'sigma'}))

		// Send
		server_event.on(data['interface'] + '_send', function(msg)
		{
			// console.log('sending...')
			worker.send(JSON.stringify(msg))
		})

		return prom
	},
	// hb client
	// Is the only one executed with a promise because needs the client to be fully connected
	// Usually is a worker
	// worker of the load balancer
	// dealer_router_HB: function(addr, identity, server_event, interface_name)
	dealer_router_HB: function(data, server_event) {
		// Defines the socket
		var worker = zmq.socket('dealer')
		worker.identity = data['identity']
		console.log("COnnecting to... " +  data['port'])
		// var prom = client_promise(worker)
		// connect....
		worker.connect('tcp://localhost:'+ data['port'])

		// For now is just connected, must wait for confirmation
		// Return the same promise and the interaces
		// Send message. msg in json format
		server_event.on(data['interface'] + '_send', function(msg)
		{
			worker.send(JSON.stringify(msg))
		})
		// receive messages already handled on the connect function
		return new Promise(function(resolve, reject){
			// Return the same promise as the connect_client
			exports.connect_Client(worker, server_event, data['interface']+'_recv').then(function()
			{
				resolve()
			},
			function(err)
			{
				reject()
			})
		})
	}
}

// To be able to export the information
exports.get_socket_zmq = function()
{
	return socket_zmq
}

// Client

global.HANDSHAKE_LIMIT = 5

global.HANDSHAKE_INTERVAL_TIME = 500

global.ALIVE_INTERVAL_TIME = 1000

// Connect function
// This is a crucial function... Connects to a remote server and sends a handhsake
// message, keep listening until it receives a confirmation message. then the promise
// is returned and it starts and itnerval that periodically sends alive messages to the server
// all the other receive events on that socket are derived to another event emitter
// In this case, that emitter is the 'server'

// It receives directly the worker connected and with the identity
exports.connect_Client = function(worker, server, interface_name) {
	return new Promise(function(resolve, reject){
		// Connection Interval
		var counts = 0
		var interval = setInterval(function(){
			// Send handshake messages
			worker.send(handshake_message())
			// Increase count and checck if surpass limit
			counts += 1
			if (counts >= HANDSHAKE_LIMIT)
			{
				reject('TimeOut')
			}
		}, HANDSHAKE_INTERVAL_TIME)
		// Connection to server and wait messages
		worker.on('message', function(raw_data){
			data = JSON.parse(raw_data)
			// Check code
			// console.log(data)
			// console.log(typeof data)
			// console.log(data['code'])
			if (data['code'] == CONFIRM_WORKER)
			{
				// console.log("bb")
				clearInterval(interval)
				// Alive interval
				setInterval(function(){
					worker.send(still_alive_message())
				}, ALIVE_INTERVAL_TIME)
				// All good so far.. return good promise
				resolve('worker works!')
			}
			else
			{
				// console.log("cc")
				// Other messages are handled by the server
				server.emit(interface_name, data)
			}
		})
	})
}

// [[SERVER]]
// All the functions to define the broker
// Important part.. the broker handle workers. It is used by the server
// and the load balancer. Try to keep it as generic as posible to be used
// in both components without modifications

global.TIMEOUT_INTERVAL_TIME = 2000

// Worker. is the one that handles his own timeout

function Worker(worker_id, evente)
{
	this.evente 	= evente
	this.worker_id 	= worker_id
	this.timeout 	= undefined
	// Start the timeou
	this.startTimeOut()
}

Worker.prototype.startTimeOut = function(first_argument) 
{
	var worker_reference = this
	this.timeout = setTimeout(function(){
		// The worker fail giving us notification of liveness
		// Emit an event on the worker list
		worker_reference.evente.emit('worker_out', worker_reference.worker_id)
	}, TIMEOUT_INTERVAL_TIME)
};

Worker.prototype.notifyAlive = function(first_argument) 
{
	// Stop the current timeout and start another one
	clearTimeout(this.timeout)
	this.startTimeOut()
};

// Worker list. Keeps a list of workers and defines helper functions
// This class could be avoided. Now, keep it, and maybe remove it later

function WorkerList(evente)
{
	this.evente = evente
	this.list   = {}

	var wlist_reference = this
	this.evente.on('worker_out', function(worker_id){
		// One of the workers reach the timeout
		// Naive version. Remove it from list and emit to the broker
		// another event in case the final user want to do something
		delete wlist_reference.list[worker_id]
		this.emit('removeWorker', worker_id)
	})
};

WorkerList.prototype.getWorker = function(worker_id) 
{
	return this.list[worker_id]
};

WorkerList.prototype.notifyAlive = function(worker_id) 
{
	if (this.getWorker(worker_id) != undefined)
	{
		this.getWorker(worker_id).notifyAlive()
	}
};

WorkerList.prototype.addWorker = function(worker_id) 
{
	if (this.getWorker(worker_id) == undefined)
	{
		// Create the worker and add him to the list
		var worker = new Worker(worker_id, this.evente)
		this.list[worker_id] = worker
		// Emit an event in the client
		this.evente.emit('addWorker', worker_id)
	}
};

// Broker

function Broker(router, evente, interface_name)
{
	// Event so that client can handle some of the actions that happen here
	this.evente = evente
	this.router = router
	// Is the name for the receive messages on the router
	this.interface_name = interface_name

	this.workers_list = new WorkerList(this.evente)
	// start the server
	this.startServer()
}

Broker.prototype.startServer = function() 
{
	var broker_reference = this
	this.router.on('message', function(){
		var args = Array.apply(null, arguments)
		// Extract id and message
		// Every message should be a json object
		var comp_id = args[0].toString()
		var msgJson = JSON.parse(args[1].toString())
		// Depends on code..
		if (msgJson['code'] == HANDSHAKE)
		{
			broker_reference.workers_list.addWorker(comp_id)
			// Send message of confirmation to worker
			broker_reference.router.send([comp_id, confirm_worker()])
		}
		else if (msgJson['code'] == ALIVE)
		{
			broker_reference.workers_list.notifyAlive(comp_id)
		}
		else
		{
			// Not related to the broker, emit to the client
			broker_reference.evente.emit(broker_reference.interface_name+'_recv', comp_id, msgJson)
		}
	})
};

// export the broker
exports.compose_Broker = function(router, evente, interface_name) 
{
	return new Broker(router, evente, interface_name)
}

