
// All other utils not related with connections and not handled by comcat

var exports = module.exports = {};

// Component name is the name assigned to the group of compoennts
// Generic name. This function will return a unique id that will compose
// that name with a random sequence of numbers and letters

exports.compose_Random_Identity = function(component_name) 
{
  return component_name + '_' + Math.random().toString(36).slice(2)
};

// [[CONNECTIONS]]

global.INTERFACE_NOT_DEFINED = 0

global.INTERFACE_DEFINED = 1

// Promise that returns true when all the connections are completed
// And the core.js program can be executed
// This function is mainly accesed by the user in core.js
// On the runtime we will do a wrap of this function
// and that is the one used by the user.
exports.allConnected = function(interface_def) 
{
	return new Promise(function(resolve, reject){
		// Check periodically the state of the interfaces
		var interval = setInterval(function(){
			var AllInterfacesDefined = true
			// console.log(interface_def)
			// Check if at least one is not defined
			for (var i in interface_def) {
				if (interface_def[i] == INTERFACE_NOT_DEFINED) {
					AllInterfacesDefined = false
				}
			}
			// Good if all the intervaces defined
			if (AllInterfacesDefined == true) {
				// Stop checking and return the promise
				clearInterval(interval)
				resolve('good')
			}
		}, 1000)
		// Do not need reject function in this case, maybe if the time
		// elapsed is too much we coudl call soemthing like.
		// Too many time waiting.. but to know the source of the block, that
		// error shoudl be raised on the connection functions..
	})
};

