#!/bin/bash

# $1 is the main directory with the package
# The others only depend from this one

tar xf $1 --strip-components=1

rm $1

# Untar all the other files
# (should be only utils)
for f in *.tar.gz
do
  tar xf "$f"
done

# Remove untared files
rm -rf *.tar.gz

# Remove execute.sh
rm execute.sh