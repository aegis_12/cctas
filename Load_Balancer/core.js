
var deployer = require('./Utils/runtime.js')

var component = require('./component.json')

var workers_list = []

// Select a random worker in workers_list
function selectRandomWorker()
{
	// Not consider the case with worker_list == 0. Do not know what would i do yet.
	return workers_list[Math.floor(Math.random()*workers_list.length)];
};

deployer.Ready(component).then(function(deployer_interface) 
{
	console.log("All ready")
	// Start listening other interfaces
	deployer_interface.on('addWorker', function(worker_id)
	{
		console.log("Add Worker ---> " + worker_id)
		// Add worker to list
		workers_list.push(worker_id)
	})

	deployer_interface.on('removeWorker', function(worker_id)
	{
		console.log("REMOVED " + worker_id)
	})

	// Backend: workers returned the message
	deployer_interface.on('out_recv', function(worker_id, msg)
	{
		console.log("DEPLOYER BACKEND ---- ")
		console.log("worker id " + worker_id)
		console.log("message ")
		console.log(msg)
		// Get the client to send back the message
		client_obj = msg['clientID']
		// new message
		n_message = {'data': msg['data']}
		// Send back the whole msg to clientid
		deployer_interface.emit('in_send', client_obj, n_message)
	})

	// Receive message from frontend clients
	deployer_interface.on('in_recv', function(client_id, msg)
	{
		console.log("DEPLOYER FRONTEND ---- ")
		console.log("client id " + client_id)
		console.log("message ")
		// Select a worker to send the data
		worker_obj = selectRandomWorker()
		console.log("Worker selected " + worker_obj)
		// msg is a message in json format
		msg['clientID'] = client_id
		// console.log("new message---")
		// console.log(msg)
		// send message
		deployer_interface.emit('out_send', worker_obj, msg)
	})
}, 
function(err)
{
	console.log("some error raised " + err)
})