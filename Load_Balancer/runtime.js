
// RUNTIME

// Requires
var comcat = require('../Utils/comcat.js')
var utils  = require('../Utils/utils.js')
var events = require('events');
var zmq = require('zmq')

var exports = module.exports = {};

// read component.json
var component = require('./component.json')

// Identity
var identity = utils.compose_Random_Identity(component['name'])
console.log('ID: ' + identity)

// Connections

var Connections = {}
Connections['server'] 		= new events.EventEmitter();
Connections['interface'] 	= new events.EventEmitter();

// Create undefined interfaces variable from component.json
// Format: {interface1_name: INTERFACE_NOT_DEFINED, interface2_name: INTERFACE_NOT_DEFINED}
var undefined_interfaces = {}

for (var item in component['interfaces'])
{
	undefined_interfaces[component['interfaces'][item]] = INTERFACE_NOT_DEFINED
}
console.log("Interface file: " + undefined_interfaces)

// export
// All connected
exports.Ready = function()
{
	return new Promise(function(resolve, reject)
	{
		// return a promise with the reference of the Connections interface
		utils.allConnected(undefined_interfaces).then(function()
		{
			resolve(Connections['interface'])
		})
	})
}

// Connect to Main server
comcat.get_socket_zmq().dealer_router_HB('tcp://127.0.0.1:9000', identity, Connections['server'], 'server').then(function()
{
	console.log("Worker conectado con el servidor principal")
}, 
function(err)
{
	console.log("not connected to server TIMEOUT: " + err)
})

// Start listening connections
Connections['server'].on('server_recv', function(data)
{
	// Data already in json format
	// console.log(data)
	if (data['code'] == SETTING_DEFINITION)
	{
		// Check interface here...
		// Check if interface left...
		// Check if interface already defined...
		if (data['zmq'] == 'sub')
		{
			// console.log("B")
			// comcat.get_socket_zmq().subscriber(data['addr'], data['tag'], Connections[data['event']], data['interface'])
			// undefined_interfaces[data['interface']] = INTERFACE_DEFINED
		}
		else if (data['zmq'] == 'router_dealer') 
		{
			//console.log(data)
			comcat.get_socket_zmq().router_dealer(data['addr'], Connections[data['event']], data['interface'])
			
			undefined_interfaces[data['interface']] = INTERFACE_DEFINED
		}
		else if (data['zmq'] == 'router_dealer_HB')
		{
			comcat.get_socket_zmq().router_dealer_HB(data['addr'], Connections[data['event']], data['interface'])
			
			undefined_interfaces[data['interface']] = INTERFACE_DEFINED
		}
		else if (data['zmq'] == 'dealer_router_HB')
		{
			comcat.get_socket_zmq().dealer_router_HB(data['addr'], identity, Connections[data['event']], data['interface']).then(function()
			{
				console.log("worker conectado")
			}, 
			function(err)
			{
				console.log("not connected to server: " + err)
			})
		}
	}
})

/*
Connections['interface'].on('addWorker', function(worker_id)
{
	console.log("add worker ---> " + worker_id)
})

Connections['interface'].on('out_recv', function(worker_id, mensaje)
{
	console.log("---------__>  out_recv " + data)
	console.log("+++++++++++++ " + mensaje)
})
*/