// bridge definition

var exports = module.exports = {};

function compose_Random_IdentityCMP (component_name) 
{
  return component_name + '_AW_' + Math.random().toString(36).slice(2)
};

portNumber = 6000

function getNewPort() {
	return portNumber++
}
/*
// Bridge has two addresses.
lb_bridge = {
	// Name used on the type attribute
	'name' : 'loadbalancer',
	// Entry point on the component lb.js
	'entry': {
		// Interface on the component. must match
		'interface' : 'in',
		'server_zmq': 'router_dealer',
		'client_zmq': 'dealer_router',
	},
	// Exit point
	'exit' : {
		'interface' : 'out',
		'server_zmq': 'router_dealer_HB',
		'client_zmq': 'dealer_router_HB',
	}
}

// Here the only important thing is the interface name
// and to knwo that this one is destination or source
edge_definition = {
	"comp": "c2",
	"interface": "in2"
}
*/
// helper function
/*
result with the format:
[
	'in' : {
		// In this case, only returns the server...
		// For the client. returns the client value
		'socket':
		'addrs':  'port_number'
	}
	...
]
*/
/*
z = [{}]

addPortToBridgeJson(lb_bridge)

x = composeInterfaceBridge(lb_bridge)

console.log(x)
*/
// y = composeInterfaceNode(lb_bridge, 'entry', edge_definition)

// console.log(composeInterfaceNode(lb_bridge, 'entry', edge_definition))

// console.log(composeInterfaceNode(lb_bridge, 'exit', edge_definition))

// Need to add first a port to the values of the bridge (json_file)
// Dinamically. so take the definition and add the entry, after that use that new definition to create the interfaces for both node and bridge
function addPortToBridgeJson(bridge_json) 
{
	// Kwown format. Only has two entries for interface. 'entry' 'exit'
	bridge_json['entry']['port'] = getNewPort()
	bridge_json['exit']['port'] = getNewPort()
};

// Takes the definition of the bridge and the component and defines the interface values with the upper format
// Takes the specific json entry for that edge we are defining
function composeInterfaceNode(bridge_interface, side, edge_interface) 
{
	// side tells you if the node is part of the source or the destination
	// Need to create only one value of interface that will be appended later
	// return as well a json file... for the interface of the node. the one defined in edge_interface
	return [{
		'interface' : edge_interface['interface'],
		'socket' : bridge_interface[side]['client_zmq'],
		'port' : bridge_interface[side]['port']
	}]
}

// Takes the definition of the bridge and defines the interface values for the node
// Seemd a bit redundant. dont wanna change it now...
function composeInterfaceBridge(bridge_interface) 
{
	// Like is a known format as well. should be trivial
	// interfc = []
	function helper(val_ref) {
		return {
			'interface': bridge_interface[val_ref]['interface'],
			'socket': bridge_interface[val_ref]['server_zmq'], 
			'port': bridge_interface[val_ref]['port']
		}
	}
	// interfc.push(helper('entry'))
	// interfc.push(helper('exit'))
	return [helper('entry'), helper('exit')]
}

// Node is created when reading the components...
// Need a function to comunicate somewhere else...
function Node(name, id, start_script)
{
	this.name = name
	// IDName will be the id on the server side
	// For nodes is the name of the component. otherwise... is the random one
	this.IDName = id
	// Connection with the bridge through the socket references
	this.WKName = null
	// Message event genereated for the server for this specific bridge WKName when defined
	this.message_event = null
	// status messages
	this.setUp = false		// Is activated. Running on a docker file
	this.running = false	// The server stuff are all correctly running
	// Interfaces data...
	this.interfc = []
	this.start_script = start_script
	// create value
	// this.create()
	this.dependsOn = []
	this.linked = []
}

var exec = require('child_process').exec;

// Create
// Main function to start the process on the docker containers and all that stuff..
Node.prototype.create = function() 
{
	// 	console.log("AAA")
	// now make exec over start_script
	exec('xterm -hold -e "node ' + this.start_script + '"', function(err) {
		if(err){ //process error
			console.log("error")
		}else{ 
			console.log("success open222")
		}
	})
};

Node.prototype.addEventlistener = function(message_event) 
{
	this.message_event = message_event
};

/*
interface_format:
[ { interface: 'in', socket: 'router_dealer', port: 6000 },
  { interface: 'out', socket: 'router_dealer_HB', port: 6001 } ]
*/
// Take the interface and create a message to send to the server. to tell him to set up all servers
// that information is on the interfaces
Node.prototype.setUpServersMessage = function () 
{
	// Do not use comcat now... instead use a better format
	return {
 		'code': 5,
		'skct': this.interfc
	}
}

// Two cases:
// Node is a bridge and new_interface is an array with two values. append both to this.interfc
// this.interfc = []   new_interface = [{},{}]
// result...> this.interfc = [{},{}]
// Node is a simple node, new interface only contains one value. the one related with the bridge
// will need to append more values later on.
Node.prototype.addInterface = function (new_interface) 
{
	this.interfc = this.interfc.concat(new_interface)
};

Node.prototype.links_dependsOn = function (nodes_ids) 
{
	this.dependsOn = this.dependsOn.concat(nodes_ids)
}

// Set the nodes associated with this bridge
Node.prototype.linked_nodes = function (nodes_ids) 
{
	// Add link to which this one is linked. when the component is running
	// We will notify to this nodes.
	this.linked = this.linked.concat(nodes_ids)
}

function simp () {
	return JSON.stringify({
		'code': 51121,
		'skct': 'aa'
	})
}

// True if all the elements they depends are true
Node.prototype.AllDependenciesGood = function() 
{
	for (var item in this.dependsOn) {
		//var depend_cmp = getNodeByIDName(that.dependsOn[item])
		console.log(that.dependsOn[item])
		//console.log(depend_cmp.running)
	}
};

Node.prototype.notify = function() 
{
	if (this.running == true) 
		return
	// console.log(this.IDName)
	// console.log("is notified")
	for (var i=0; i<this.dependsOn.length; i++) {
		m = getNodeByIDName(this.dependsOn[i])
		if (m.running == false) {
			return
		}
	}
	// reach this point means all requeriments are fulfilled
	// start the process
	this.create()
};

// Associate to this bridge a worker socket and start to listen messages
Node.prototype.associate_Worker = function (WKName) 
{
	this.WKName = WKName
	// listen
	this.message_event.on(WKName, function(data){
		// All messages from the bridge will be handled here...
		// Use later somewhere else
		// console.log("second")
		// console.log(data)
	})

	// send to the worker the data for the sockets..
	// this.sendMessage(simp())
	var that = this

	return new Promise(function (resolve, reject) {
		// this.sendMessage(this.setUpServersMessage())
		that.startUpProcess().then(function () 
		{
			console.log("notify")
			that.setUp = true
			that.running = true
			// Notify to linked nodes that this one is running. is their
			// job to check if depends are running...
			for (var linked in that.linked)
			{
				var linked_cmp = getNodeByIDName(that.linked[linked])
				linked_cmp.notify()
			}
			resolve(that.name)
		})
	})
	// make the start now....
	// in form of promise....
}

global.CONFIRM_SETTING = 6

// Send data for intialization..
// Wait for the response and confirmation()
// this.sendMessage(this.setUpServersMessage())
Node.prototype.startUpProcess = function() 
{
	// return promise, when promises fulfilled send notify messages
	this.sendMessage(this.setUpServersMessage())
	var that = this
	return new Promise(function (resolve, reject) {
		that.message_event.on(that.WKName, function (data) {
			if (data['code'] == CONFIRM_SETTING) 
			{
				resolve("")
			}
			// console.log(event_reference
		})
	})
};

Node.prototype.sendMessage = function(dataJson) 
{
	// console.log("assa")
	this.message_event.emit(this.WKName + '_send', dataJson)
};

Node.prototype.addInterfaceBridge = function(bridge_interface) 
{
	this.addInterface(composeInterfaceBridge(bridge_interface))
};

Node.prototype.addInterfaceNode = function(bridge_interface, site, cmp_interface) 
{
	this.addInterface(composeInterfaceNode(bridge_interface, site, cmp_interface))
};

function getNodeByIDName (name) {
	for (var item in nodes) {
		if (nodes[item].IDName == name) {
			return nodes[item]
		}
	}	
}

function getNodeByName (name) {
	for (var item in nodes) {
		if (nodes[item].name == name) {
			return nodes[item]
		}
	}	
}

// return the id
exports.getNodeByName = function (name) 
{
	return getNodeByName(name)
}

nodes = {}

exports.createNewBridge = function (name, start_script) 
{
	// create node and append
	var n = new Node(name, compose_Random_IdentityCMP(name), start_script)
	nodes[n.IDName] = n
	// console.log(nodes)
	return nodes[n.IDName]
}

exports.createNode = function (name, start_script) 
{
	// create node and append
	var n = new Node(name, name, start_script)
	nodes[n.IDName] = n
	// console.log(nodes)
	return nodes[n.IDName]
}

exports.addNodeInterfaces = function (id, bridge_interface, cmp_interface) 
{
	nodes[id].addInterface(composeInterfaceNode(bridge_interface, cmp_interface))
}

exports.addBridgeInterfaces = function (id, bridge_interface) 
{
	// change the bridge interface and add it
	// getNode(name).addInterface(composeInterfaceBridge(bridge_interface))
	nodes[id].addInterface(composeInterfaceBridge(bridge_interface))
	// console.log(nodes)
}

exports.addEventlistener = function (id, event_listener) 
{
	nodes[id].addEventlistener(event_listener)
}

exports.associate_Worker = function (id, WKName) 
{
	nodes[id].associate_Worker(WKName)
}

exports.show = function (argument) {
	console.log(nodes)
}

// There are many
exports.getFirstAvailableNode = function (name) 
{
	// search for node...
	for (var item in nodes) {
		if (nodes[item].name == name) {
			return nodes[item]
		}
	}
}

// Go all over the project and check those modules
// without dependencies.
exports.RunNoDependentComponents = function () 
{
	for (var i in nodes) {
		var cmp = nodes[i]
		if (cmp.dependsOn.length == 0) {
			// console.log("--Empty--")
			// console.log(cmp.IDName)
			cmp.create()
		}
	}
}