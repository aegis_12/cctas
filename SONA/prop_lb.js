// simple load balancer to check if some attributes work

var comcat = require('../Utils/comcat.js')
var utils  = require('../Utils/utils.js')
var events = require('events');
var zmq = require('zmq')

// Connections

var Connections = {}
Connections['server'] 		= new events.EventEmitter();
Connections['interface'] 	= new events.EventEmitter();

// Create the part of the heartbeat
// like the server...

comcat.get_socket_zmq().router_dealer_HB('tcp://*:6000',Connections['server'],'server')

Connections['server'].on('addWorker', function(worker_id)
{
	console.log("ADD: " + worker_id)
})
