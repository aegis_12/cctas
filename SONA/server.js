
// Server as main server deployer

var comcat = require('../Utils/comcat.js')
var utils  = require('../Utils/utils.js')
var mind = require('./bridge.js')
var events = require('events');
var zmq = require('zmq')
var logger = require('../Utils/log.js')

function simp () {
	return JSON.stringify({
		'code': 511,
		'skct': 'aa'
	})
}

portNumber = 6000

function getNewPort() {
	return portNumber++
}

function addPortToBridgeJson(bridge_json) 
{
	bridge_json_aux = bridge_json
	// Kwown format. Only has two entries for interface. 'entry' 'exit'
	bridge_json_aux['entry']['port'] = getNewPort().toString();
	bridge_json_aux['exit']['port'] = getNewPort().toString();
	// return value
	return bridge_json_aux
};

// Some defintions
lb_bridge = {
	// Name used on the type attribute
	'name' : 'loadbalancer',
	// Startup code
	'path' : './Load_Balancer/core.js',
	// Entry point on the component lb.js
	'entry': {
		// Interface on the component. must match
		'interface' : 'in',
		'server_zmq': 'router_dealer',
		'client_zmq': 'dealer_router',
	},
	// Exit point
	'exit' : {
		'interface' : 'out',
		'server_zmq': 'router_dealer_HB',
		'client_zmq': 'dealer_router_HB',
	}
}

pubsub_bridge = {
	// Name used on the type attribute
	'name' : 'pubsub',
	// startup code
	'path' : './PubSub/core.js',
	// Entry point on the component lb.js
	'entry': {
		// Interface on the component. must match
		'interface' : 'in',
		'server_zmq': 'pull_server',
		'client_zmq': 'push_socket',
	},
	// Exit point
	'exit' : {
		'interface' : 'out',
		'server_zmq': 'publisher',
		'client_zmq': 'subscriber',
	}
}

components = [
	{
		'name': 'first',
		'path': './Cmp_one/core.js',
	},
	{
		'name': 'second',
		'path': './Cmp_second/core.js',
	},
	{
		'name': 'third',
		'path': './Cmp_third/core.js',
	}
]

// mappings of the bridge..
// used to map types of the graph definition
bridge_map = 
{
	'loadbalancer': lb_bridge,
	'pubsub': pubsub_bridge,
}

graph = [
	{
		'entry': {
			'name': 'first',
			'interface': 'out',
		},
		'exit': {
			'name': 'second',
			'interface': 'in',
		},
		'type': {
			'name': 'loadbalancer', 
		}
	},
	{
		'entry': {
			'name': 'second',
			'interface': 'out',
		},
		'exit': {
			'name': 'third',
			'interface': 'in',
		},
		'type': {
			'name': 'pubsub', 
		}
	},
]

// Use the component and the edge definition. get reference of all the components
// create the interfaces and start stuff..
function createEdgeReference (bridge_def, edge, local_events) {
	// Set up the address address with the port
	bridge_json = addPortToBridgeJson(bridge_def)
	// Set up bridge.
	bridge = mind.createNewBridge(bridge_json['name'],bridge_json['path'])
	bridge.addEventlistener(local_events)
	bridge.addInterfaceBridge(bridge_json)
	// site: 'entry' | 'exit'
	function setUpEdgeInterface (site) {
		// site from edge component. and retrieve the reference node.
		node = mind.getNodeByName(edge[site]['name'])
		node.addInterfaceNode(bridge_json, site, edge[site])
		node.links_dependsOn([bridge.IDName])
		// And add up the reference to the bridge
		bridge.linked_nodes([node.IDName])
	}
	// create both entries for entry and exit
	setUpEdgeInterface('entry')
	setUpEdgeInterface('exit')
	// Add linked to bridge
}

// Main router listening to the components
var router = zmq.socket('router')
router.bindSync('tcp://*:9000');

// Event to listen messages
server_events = new events.EventEmitter()

local_events = new events.EventEmitter()
// Start the broker
brk = comcat.compose_Broker(router, server_events, 'message')

// Component definition to nodes...
for (var i in components) {
	cmp = components[i]
	// create the node and add all related values
	node = mind.createNode(cmp['name'],cmp['path'])
	node.addEventlistener(local_events)
}

for (var indx in graph) {
	var edge = graph[indx]
	var bridge_def = bridge_map[edge['type']['name']]
	createEdgeReference(bridge_def, edge, local_events)
}

/**
*	Start all the components without any dependable items. By definition, the bridges
*	do not have any dependecy, those will be the first elements to create
*/
mind.RunNoDependentComponents()

server_events.on('addWorker', function(worker_id) 
{
	/**
	*	Worker id comes from the component as is composed with the worker name
	*	and alphabetic number combination of the form: worker_3fa3fr4n6g. 
	*	The first strings after the _ compose the name
	*/
	worker_name = worker_id.split('_')[0]

	// Reference of the node
	node = mind.getFirstAvailableNode(worker_name)

	/**
	*	Wrapper. To send messages to the worker. Local_Events is passed to
	*	all the nodes as 'message_event'. To send messages emit:
	*	local_events.emit(worker_id+'_send', json_data)
	*/
	local_events.on(worker_id + '_send', function (dataJson) 
	{
		router.send([worker_id, JSON.stringify(dataJson)])
	})

	/**
	*	At this point, node does not have any reference to a worker. i.e. worker_id
	*	associate to this node the workerid and start to listen to that socket on the node
	*	Delegate all the main communication functions on the node
	*	This function will send a configuration messages and wait for a confirmation code
	*/
	node.associate_Worker(worker_id).then(function(name) 
	{
		// Connection and initialization was correct
		logger.info("Node " + name + " set up")
	})
})

/*
*	Wrapper that listens messages from the workers, the messages are passed to the nodes
*	through local_events that handle the messages
*/
server_events.on('message_recv', function(worker_id, msg)
{
	local_events.emit(worker_id, msg)
})