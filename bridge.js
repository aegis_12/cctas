// bridge definition

function compose_Random_IdentityCMP (component_name) 
{
  return component_name + '_AW_' + Math.random().toString(36).slice(2)
};

portNumber = 6000

function getNewPort() {
	return portNumber++
}

// Bridge has two addresses.
lb_bridge = {
	// Name used on the type attribute
	'name' : 'loadbalancer',
	// Entry point on the component lb.js
	'entry': {
		// Interface on the component. must match
		'interface' : 'in',
		'server_zmq': 'router_dealer',
		'client_zmq': 'dealer_router',
	},
	// Exit point
	'exit' : {
		'interface' : 'out',
		'server_zmq': 'router_dealer_HB',
		'client_zmq': 'dealer_router_HB',
	}
}

// Here the only important thing is the interface name
// and to knwo that this one is destination or source
edge_definition = {
	"comp": "c2",
	"interface": "in2"
}

// helper function
/*
result with the format:
[
	'in' : {
		// In this case, only returns the server...
		// For the client. returns the client value
		'socket':
		'addrs':  'port_number'
	}
	...
]
*/

addPortToBridgeJson(lb_bridge)

x = composeInterfaceBridge(lb_bridge)

console.log(x)

y = composeInterfaceNode(lb_bridge, 'entry', edge_definition)

console.log(composeInterfaceNode(lb_bridge, 'entry', edge_definition))

console.log(composeInterfaceNode(lb_bridge, 'exit', edge_definition))

// Need to add first a port to the values of the bridge (json_file)
// Dinamically. so take the definition and add the entry, after that use that new definition to create the interfaces for both node and bridge
function addPortToBridgeJson(bridge_json) 
{
	// Kwown format. Only has two entries for interface. 'entry' 'exit'
	bridge_json['entry']['port'] = getNewPort()
	bridge_json['exit']['port'] = getNewPort()
};

// Takes the definition of the bridge and the component and defines the interface values with the upper format
// Takes the specific json entry for that edge we are defining
function composeInterfaceNode(bridge_interface, side, edge_interface) 
{
	// side tells you if the node is part of the source or the destination
	// Need to create only one value of interface that will be appended later
	// return as well a json file... for the interface of the node. the one defined in edge_interface
	return {
		'interface' : edge_interface['interface'],
		'socket' : bridge_interface[side]['client_zmq'],
		'port' : bridge_interface[side]['port']
	}
}

// Takes the definition of the bridge and defines the interface values for the node
// Seemd a bit redundant. dont wanna change it now...
function composeInterfaceBridge(bridge_interface) 
{
	// Like is a known format as well. should be trivial
	interfc = []
	function helper(val_ref) {
		return {
			'interface': bridge_interface[val_ref]['interface'],
			'socket': bridge_interface[val_ref]['server_zmq'], 
			'port': bridge_interface[val_ref]['port']
		}
	}
	interfc.push(helper('entry'))
	interfc.push(helper('exit'))
	return interfc
}

function Node(name, interfc)
{
	this.name = name
	// IDName will be the id on the server side
	this.IDName = compose_Random_IdentityCMP(name)
	// Connection with the bridge through the socket references
	this.WKName = null
	// Message event genereated for the server for this specific bridge WKName when defined
	this.message_event = null
	// status messages
	this.setUp = false		// Is activated. Running on a docker file
	this.running = false	// The server stuff are all correctly running
	// Interfaces data...
	this.interfc = interfc
}

function links_dependsOn (nodes_ids) 
{
	this.dependsOn = nodes_ids
}

// Set the nodes associated with this bridge
function linked_nodes (nodes_ids) 
{
	// Add link to which this one is linked. when the component is running
	// We will notify to this nodes.
	this.linked = nodes_ids
}

// Associate to this bridge a worker socket and start to listen messages
function associate_Worker (WKName, message_event) 
{
	this.WKName = WKName
	this.message_event = message_event
	// listen
	this.message_event.on(WKName + '_recv', function(){
		// All messages from the bridge will be handled here...
	})
}