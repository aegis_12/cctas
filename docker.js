
// docker...

var exec = require('child_process').exec;

// Promise used for connecting with docker
function executeCommand(command)
{
	return new Promise(function(resolve, reject)
	{
		exec(command, function(error, stdout, stderr)
		{
			if (error !== null) {
				reject(error)
			}
			// It is adding a new line at the end. remove it
			resolve(stdout.replace(/\n$/, ''))
		})
	})
};

function Docker(image_base)
{
	this.image_base = image_base
	this.ID = null
}

// Run it and keep it Alive
Docker.prototype.create = function() 
{
	var docker_reference = this

	return new Promise(function(resolve, reject){
		// Create + run
		executeCommand('docker run -dt ' + docker_reference.image_base + ' bash').then(function(res)
		{
			// The real id are the first 12 digits
			docker_reference.ID = res.substring(0,12)
			resolve(res)
		},
		function(err)
		{
			// console.log(err)
			reject(err)
		})		
	})
	// sudo docker run -dt nvm/base
};

// Copy many files at the same time. Return Q.all Promise...
// CopyList format: [ [file1, destiny1], [file2, destiny2], [file3, destiny3]]
Docker.prototype.copySeveralFiles = function(copyFilesList)
{
	var promise_list = []
	for (var indx in copyFilesList) 
	{
		this.copyFile(copyFilesList[indx][0], copyFilesList[indx][1])
	}
	// call all the promises
	return Promise.all(promise_list)
};

Docker.prototype.copyFile = function(host_file, dest_directory) 
{
	// sudo docker cp host_file this.ID:dest_directory
	return executeCommand('docker cp ' + host_file + ' ' + this.ID + ':' + dest_directory + host_file)
};

// Run a command to untar all sended files
// Execute execute.sh command
// Need the name of the tar file that stores all the data
Docker.prototype.execute = function(folder_name)
{
	return executeCommand('docker exec ' + this.ID + ' bash /root/execute.sh ' + folder_name)
};

d = new Docker('nvm/base2')

files_list = [
	['./utils.tar.gz','/root/'],
	['./execute.sh','/root/'],
	['./lb.tar.gz','/root/']
]

d.create().then(function(ID){
	console.log(ID)
	console.log(d.ID)

	d.copySeveralFiles(files_list).then(function(res){
		console.log('Succed copying files ' + res)

		d.execute('lb.tar.gz').then(function(res){
			console.log('untared')
		}, function(err){
			console.log('error2')
		})

	}, function(err){
		console.log(err + 'error 1')
	})
}, function(err){
	console.log("errr " + err)
})