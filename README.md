# Cloud Scheduler #

Class project for CCTAS (Master)

The scheduler takes a JSON file that defines a graph where each vertex represents a function (javascript) and the edge represents a communication channel (load balancer, pubsub...). Each vertex has a replication factor with the number of replicas to provision.

At runtime, the scheduler is able to deploy the services in order to ensure that the graph dependencies are fulfilled. Every functions is injected into a docker image.

The default communication channels (load balancer and pubsub) act like any other service in the system so that the user can program his own channel.