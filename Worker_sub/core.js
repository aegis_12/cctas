
var deployer = require('./Utils/runtime.js')

var component = require('./component.json')

deployer.Ready(component).then(function(deployer_interface) 
{
	console.log("All ready")
	// Receive message from frontend clients
	deployer_interface.on('in_recv', function(msg)
	{
		console.log(msg)
	})
}, 
function(err)
{
	console.log("some error raised " + err)
})