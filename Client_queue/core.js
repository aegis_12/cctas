
var deployer = require('./Utils/runtime.js')

var component = require('./component.json')

var workers_list = []

function other_simple_message() 
{
	return {
		'code': "1999",
		'mees': "alpha"
	}
}

deployer.Ready(component).then(function(deployer_interface) 
{
	console.log("All ready")
	
	deployer_interface.emit('out_send', other_simple_message())

	// Backend: workers returned the message
	/*deployer_interface.on('out_recv', function(msg)
	{
		console.log('receiveed ')
		console.log(msg)
	})*/
}, 
function(err)
{
	console.log("some error raised " + err)
})