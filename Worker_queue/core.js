
var deployer = require('./Utils/runtime.js')

var component = require('./component.json')

function other_simple_message() 
{
	return {
		'code': "1asdasd999",
		'mees': "alsasdapha"
	}
}

deployer.Ready(component).then(function(deployer_interface) 
{
	deployer_interface.emit('in_send', other_simple_message())
	console.log("All ready")
	// Receive message from queue broker
	deployer_interface.on('in_recv', function(msg)
	{
		console.log("-----------------")
		console.log(msg)
	})
}, 
function(err)
{
	console.log("some error raised " + err)
})