
var deployer = require('./Utils/runtime.js')

var component = require('./component.json')

function other_simple_message() 
{
	return {
		'tag': "beta",
		'mees': "alphaAAAAA"
	}
}

deployer.Ready(component).then(function(deployer_interface) 
{
	console.log("All ready")
	// Receive message from frontend clients
	deployer_interface.on('in_recv', function(msg)
	{
		console.log("-----------------")
		console.log(msg)
		// Select a worker to send the data
		// compose new message
		new_msg = {'clientID':msg['clientID'], 'data':'beta'}
		// send it back
		deployer_interface.emit('in_send', new_msg)

		deployer_interface.emit('out_send', other_simple_message())
	})
}, 
function(err)
{
	console.log("some error raised " + err)
})