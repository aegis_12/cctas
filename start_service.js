
var param_transform = require('./transform.js')
// console.log(param_transform)
// read the deployment file
var dep = require("./deployment.json")

// Start the service
service_uri = dep['uri']
var serv = require(service_uri)

// Check params and get the final combination
service_params = checkParamsCorrectness(dep['params'], serv['attributes'])

// console.log(params)

// Helper function
function newComponent(component_json) {
	return new Component(component_json['uri'], component_json['alias'], component_json['params'])
}

// Create a component object and initialize params
function Component(uri, alias, raw_params) {
	this.uri = uri
	this.alias = alias
	this.raw_params = raw_params
	// Executre composeParams() to create initParams
	// Otherwise will raise Error
	this.init_raw_params = null
}

// Take the params and initialize them
// Use service_params to initialize
// And transform library
Component.prototype.composeParams = function() {
	// console.log(service_params)
	this.init_raw_params = {}
	// var n_params = {}
	for (var key in this.raw_params) {
		if (this.raw_params[key][0] == "transform") {
			// get the function in second parameter and invoke
			var transform_function = this.raw_params[key][1]
			// console.log(transform_function)
			if (param_transform[transform_function] == undefined){
				// raise error. does not exists the function
				throw new Error('Transofrm function ' + transform_function + ' not found');
			} else {
				// var x = param_transform[transform_function](service_params)
				this.init_raw_params[key] = param_transform[transform_function](service_params)
			}
		} else {
			// That operation is not supported. so far. only trnasofmed
			throw new Error('Transofrm operation ' + this.raw_params[key][0] + ' not found');
		}
	}
}

// From the uri..
// Download the code of each component and check the values
// are valid with the deifition on the service.json
Component.prototype.adquireComponent = function(argument) {
	// Get the uri and retrieve the object
	// Fill the rest of the object with more parameters
	// By now. uri is a direction ./component.json relative to this filesystem
	var component_json = require(this.uri)
	// main file
	this.main = component_json['main']
	// interfaces
	this.interface = component_json['interface']
	// real_init params
	// params is the final object, considere the default values and the transformed
	// ones taked from the service
	// console.log("----")
	// console.log(this.init_raw_params)
	// These are the values passed to the file on the docker
	this.init_params = checkParamsCorrectness(this.init_raw_params, component_json['attributes'])
}

// This will have checker functions to check the correctness of the definition values
function Component_Group() {
	// Group of components extracted from the service
	this.group = []
}

Component_Group.prototype.addComponent = function(cmp) {
	var comp = newComponent(cmp)
	comp.composeParams()
	comp.adquireComponent()
	// console.log(comp)
	// Add to list
	this.group.push(comp)
}

// Helper function
Component_Group.prototype.getComponent = function(component_alias) {
	// raise error if component alias does not exists
	for (var i=0; i<this.group.length; i++){
		if (this.group[i].alias == component_alias) {
			return this.group[i]
		}
	}
	// raise error.
	throw new Error('No component found with alias ' + component_alias);
}

// Return the type of the interface
// return undefined if does not exists
// Same function to check, input interface and output. just change the value in input_output (input|output)
Component_Group.prototype.checkInterface = function(component_alias, input_output, interface_name) {
	var component = this.getComponent(component_alias)
	// console.log(component)
	if (['input','output'].indexOf(input_output) == -1) {
		throw new Error('Interface must be input or output. Found ' + input_output);
	}
	// Check if input_output is input or ouput.. otherwise raise error
	var interface = component['interface'][input_output]
	// Check interface name, if does not exists return null, error will be raised in other function
	// console.log(interface)
	return interface[interface_name]
}

// Create the group..
var grp = new Component_Group()

for (var i=0; i<serv['components'].length; i++) {
	// Add component and initialize
	// console.log(serv['components'][i])
	grp.addComponent(serv['components'][i])
}

// Check for errors in the service graph defition and interfaces
// Interfaces, in, out...
checkServiceEntryInterface(serv['interface'])

// Check graph
checkServiceGraph(serv['graph'])

// No errors. All correct so far...

// Input interface correct?
function checkServiceEntryInterface(entry_interface) {
	var entry_interface = grp.checkInterface(entry_interface['comp'], 'input', entry_interface['input'])
	if (entry_interface == undefined) {
		throw new Error('Entry interface ' + comp_interface + ' not defined on component ' + comp_id);
	}
}

// Check the correctness of the graph
function checkServiceGraph(graph) {
	for (var i=0; i<graph.length; i++) {
		// Get values from graph
		var initial = graph[i]['initial']
		var destination = graph[i]['destination']
		// Check if not undefined
		if (initial == undefined) {
			throw new Error('Error in graph. No initial direction');
		}
		if (destination == undefined) {
			throw new Error('Error in graph. No destination direction');
		}
		// Get the interface from the components
		var init = grp.checkInterface(initial['comp'], 'output', initial['interface'])
		var dest = grp.checkInterface(destination['comp'], 'input', destination['interface'])
		// Check all valid interfaces
		if (init == undefined) {
			throw new Error('Graph-Edge: Initial interface' + initial['interface'] + ' not defined in component ' + initial['comp']);
		}
		if (dest == undefined) {
			throw new Error('Graph-Edge: Destination interface' + destination['interface'] + ' not defined in component ' + destination['comp']);
		}
		// Check if type is correct
		if (init != dest) {
			throw new Error('Graph-Edge: Component-interface ['+initial["comp"]+'-'+initial["interface"]+'] (type '+init+') does not match with Component-interface ['+destination["comp"]+'-'+destination["interface"]+'] (type '+dest+'))');
		}
	}
}

// Check if the params in the deployment have a correspond param on the service
// assert function. if not correct raise error and stop service
// Return a list of real params for the service, considering both the default
// and the ones passed by the deployment
function checkParamsCorrectness(dep_init, serv_params) {
	// console.log(dep_init)
	final_params = {}
	// console.log(serv_params)
	for (var key in serv_params){
		// Check if there is specification in dep_init
		// console.log(key)
		dep_value = dep_init[key]
		if (dep_value == undefined) {
			// Get the default value. if NONE. raise error
			if (serv_params[key][1] == null) {
				// raise error
				throw new Error('Error in attribute assignation ' + key + ' non default value');
			}
			// Set the default value
			final_params[key] = serv_params[key][1]
			// console.log(key + ' - ' + serv_params[key][1])
		} else {
			// Check the type
			if (typeof dep_value == serv_params[key][0]) {
				// Get that value
				final_params[key] = dep_value
				// console.log(key + ' - ' + dep_value)
				// console.log("Mismatch types with " + key)
			} else {
				// Mismatch types... raise error
				throw new Error('Error in attribute assignation ' + key + ' mismatch types');
			}
		}
		// console.log(typeof dep_value)
		// console.log("----")
		// return final_params
	}
	return final_params
}